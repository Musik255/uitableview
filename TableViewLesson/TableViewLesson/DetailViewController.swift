//
//  DetailViewController.swift
//  TableViewLesson
//
//  Created by Павел Чвыров on 28.07.2023.
//

import UIKit




class DetailViewController: UIViewController {

    
    @IBOutlet private weak var nameLabel: UILabel!
    
    @IBOutlet private weak var ageLabel: UILabel!
    
    @IBOutlet weak var genderLabel: UILabel!
    
    
    var user: UserModel!
    weak var delegate: ViewControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = user.title
        ageLabel.text = user.age
        genderLabel.text = user.gender.rawValue
        
        nameLabel.sizeToFit()
        ageLabel.sizeToFit()
        genderLabel.sizeToFit()
        
        
        changeData()
        
        
    }
    
    private func changeData(){
        
        guard let ageInt = Int(user.age) else { return }
        
        user.age = String(ageInt + 100)
        
        delegate?.didChangeUser(user)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
