//
//  TableViewController.swift
//  TableViewLesson
//
//  Created by Павел Чвыров on 27.07.2023.
//

import UIKit

class TableViewController: UITableViewController {

    var users : [UserModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        users.append(UserModel(title: Names.test_User1.rawValue, age: "18", gender: .male))
        users.append(UserModel(title: Names.test_User2.rawValue, age: "19", gender: .male))
        users.append(UserModel(title: Names.test_User3.rawValue, age: "20", gender: .female))
        users.append(UserModel(title: Names.test_User4.rawValue, age: "21", gender: .female))
        users.append(UserModel(title: Names.test_User5.rawValue, age: "22", gender: .male))
        users.append(UserModel(title: Names.test_User6.rawValue, age: "23", gender: .female))

    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return users.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CustomTableViewCell.cellIdentifier(), for: indexPath)//переиспользуются ячейки, должны оч быстро подгружаться
        
        let model = users[indexPath.row]
        //Configure the cell...
        var content = cell.defaultContentConfiguration()
        content.text = "Name: " + model.title
        content.secondaryText = "Age: " + model.age
        
        cell.contentConfiguration = content
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
