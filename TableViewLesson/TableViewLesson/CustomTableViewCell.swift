//
//  CustomTableViewCell.swift
//  TableViewLesson
//
//  Created by Павел Чвыров on 28.07.2023.
//

import UIKit

protocol CustomCell {
    
    static func cellNib() -> UINib?
    
    static func cellIdentifier() -> String
}

protocol CustomCellDelegate: AnyObject {
    
    func didPressAction(for cell: UITableViewCell)
}

class CustomTableViewCell: UITableViewCell, CustomCell {

    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    
    weak var delegate: CustomCellDelegate?
    
    
    
    
    func configure(with user: UserModel, delegate: CustomCellDelegate){
        
        label1.text = user.title
        label2.text = user.age
        
        self.delegate = delegate
    }
    
    
    static func cellNib() -> UINib? {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
    static func cellIdentifier() -> String {
        return String(describing: self)
    }
    
    
    @IBAction func buttonAction(_ sender: Any) {
        delegate?.didPressAction(for: self)
    }
    
    
}


