//
//  User.swift
//  TableViewLesson
//
//  Created by Павел Чвыров on 27.07.2023.
//

import Foundation

enum Gender : String{
    case male = "Мужчина"
    case female = "Женщина"
}


struct UserModel{
    var title : String
    var age : String
    var gender : Gender
}
