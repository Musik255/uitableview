//
//  TableViewExtension.swift
//  TableViewLesson
//
//  Created by Павел Чвыров on 28.07.2023.
//

import Foundation
import UIKit
extension UITableView {
    
    func registerCustomCell(_ cell: CustomCell.Type){
        self.register(cell.cellNib(), forCellReuseIdentifier: cell.cellIdentifier())
    }
    
}
