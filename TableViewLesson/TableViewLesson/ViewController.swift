//
//  ViewController.swift
//  TableViewLesson
//
//  Created by Павел Чвыров on 27.07.2023.
//

import UIKit

protocol ViewControllerDelegate: AnyObject{
    func didChangeUser(_ user: UserModel)
}

enum Names : String{
    case test_User1 = "test_User1"
    case test_User2 = "test_User2"
    case test_User3 = "test_User3"
    case test_User4 = "test_User4"
    case test_User5 = "test_User5"
    case test_User6 = "test_User6"
}



class ViewController: UIViewController,
                        UITableViewDelegate,
                        UITableViewDataSource,
                        CustomCellDelegate,
                        ViewControllerDelegate{
    
    

    
    @IBOutlet weak var tableView: UITableView!
    
    var users : [UserModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        users.append(UserModel(title: Names.test_User1.rawValue, age: "18", gender: .male))
        users.append(UserModel(title: Names.test_User2.rawValue, age: "19", gender: .male))
        users.append(UserModel(title: Names.test_User3.rawValue, age: "20", gender: .female))
        users.append(UserModel(title: Names.test_User4.rawValue, age: "21", gender: .female))
        users.append(UserModel(title: Names.test_User5.rawValue, age: "22", gender: .male))
        users.append(UserModel(title: Names.test_User6.rawValue, age: "23", gender: .female))

        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.registerCustomCell(CustomTableViewCell.self)

        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CustomTableViewCell.cellIdentifier(), for: indexPath) as! CustomTableViewCell
        //переиспользуются ячейки, должны оч быстро подгружаться
        
        
        
        let model = users[indexPath.row]
        cell.configure(with: model, delegate: self)
     
    
//        var content = cell.defaultContentConfiguration()
//        content.text = "Name: " + model.title
//        content.secondaryText = "Age: " + model.age
//        content.image = UIImage(named: "image1") //так можно, но лучеше через литералы
//        content.image = #imageLiteral(resourceName: "D6C2D054-872F-4D71-A75C-86D3AC57C399")
//        cell.contentConfiguration = content
//        cell.imageView
        
        
        return cell
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 120
//        //че ваще, ниче не понял, нашел тут -> https://www.youtube.com/watch?v=R2Ng8Vj2yhY&ab_channel=iOSAcademy
//        //возможно дальше пойму
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        tableView.deselectRow(at: indexPath, animated: true)//убрать метку что ты выбрал до этого при нажатии назад
//
//        let model = users[indexPath.row]
//
//        performSegue(withIdentifier: "datailIdentifier", sender: model)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "datailIdentifier", let userModel = sender as? UserModel {
            
            let destController = segue.destination as! DetailViewController
            destController.user = userModel
            destController.delegate = self
        }
    }
    
    func didPressAction(for cell: UITableViewCell) {
        
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        
    
        let model = users[indexPath.row]
        
        performSegue(withIdentifier: "datailIdentifier", sender: model)
    }

    
    func didChangeUser(_ user: UserModel) {
        
        if let userValue = users.enumerated().first(where: { $0.element.title == user.title}){
            
            users.remove(at: userValue.offset)
            users.insert(user, at: userValue.offset)
            
            tableView.reloadData()
        }
        
    }
}

